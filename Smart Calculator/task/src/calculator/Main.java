package calculator;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        int[] elements = Arrays.stream(text.split(" ")).mapToInt(Integer::parseInt).toArray();
        int result = Arrays.stream(elements).sum();
        System.out.println(result);
    }
}
